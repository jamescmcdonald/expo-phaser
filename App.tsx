import { StatusBar } from 'expo-status-bar';
import Phaser from 'phaser';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// If Phaser is detected, return a string including the version, otherwise indicate it's unavailable
function getPhaserText() {
  if (Phaser && "VERSION" in Phaser) {
    return `Phaser version is at ${Phaser.VERSION}`;
  }

  return `Phaser is not available`
}

export default function App() {
  return (
    <View style={styles.container}>
      <Text>I edited App.tsx for my first commit!</Text>
      <Text>{getPhaserText()}</Text>
      <StatusBar style="dark" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
